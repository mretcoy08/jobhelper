<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobpost extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("crud_model");
        
    
        
    }

	public function index()
	{
		$this->load->view('template/jh_template.php');
    }
    
    public function post_job()
    {
        $data = [
            "job_title" => clean_data(post("job_title")),
            "company" => clean_data(post("company")),
            "job_type" => clean_data(post("job-type")),
            "region" => clean_data(post("region")),
            "province" => clean_data(post("province")),
            "city" => clean_data(post("city")),
            "job_description" => clean_data(post("job_description")),
    
         ];
    
         $query = $this->crud_model->insert("job_post",$data);
    
        echo json_encode($data);
    }

}
