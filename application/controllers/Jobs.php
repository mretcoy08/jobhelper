<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobpost extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("crud_model");
        
    
        
    }

	public function index()
	{
		$this->load->view('template/jh_template.php');
    }
    
    public function search_job()
    {
        $searchDataJob = clean_data(post("search_job"));
        $searchDataLocation = clean_data(post("search_location"));

        $query = $this->crud_model->search_job($searchData,$searchDataLocation);

        return $query;

    }

    public function jobs()
    {

        $query = $this->crud_model->select("job_post","*");
        
        return $query;
    }
}
