<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("crud_model"); 
    }

	// public function index()
	// {
	// 	$this->load->view('template/jh_template.php');
    // }
    
    public function login()
    {
        $username = clean_data(post("username"));
        $password = md5(post("password"));

        $query = $this->crud_model->login($username,$password); 
    }

    public function logout()  
    {  
      session_destroy();
      $this->session->unset_userdata('role','id','full_name');  
      redirect(base_url() . 'Login');  
    } 
}
