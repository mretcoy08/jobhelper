<?php
class Crud_model extends CI_Model{



    public function login($username,$password)
	{	
		$query = $this->db->select('password')
						->from('users')
						->where('username =', $username)
						->get();
			$data = [];
			$userData = [];
			$password = $password;
		if($query->num_rows()>0){
			$testpassword;
			foreach($query->result() as $k){
				$testPassword = $k->password;
			}
			if($testPassword == $password){
				$query = $this->db->select('*')
								->from('users')
								->where('username =',$username)
								->where('password =', $password)
								->get();
				if($query->num_rows()>0){
					foreach($query->result() as $k)
					{
						$userData['fullname'] = $k->first_name." ".$k->middle_name." ".$k->last_name." ".$k->suffix_name;
						$userData['role'] = $k->role;
						$userData['id'] = $k->id;
					}

					$this->session->set_userdata($userData);
					$userData['msg'] = "loginSuccess";
					return $userData;
				}
			}
			else{
				$userData ['msg'] = "passwordError";
				return $userData;
			}
		}
		else{
			$userData ['msg'] = "usernameError";
			return $userData;
		}
	}


    public function insert($table,$data)
    {
        $query = $this->db->insert($table,$data);

        return $query;
    }

    public function insertWithId($table,$data)
	{
		$query = $this->db->insert($table,$data);
		$last_id = $this->db->insert_id();
		
		return $last_id;
		
	}

    public function select($table,$data,$where="")
    {
        $this->db->select($data)
                        ->table($table);
                        if($where!="")
                        {
                            $this->db->where($where);
                        }
                        $query = $this->db->get();
                        
        return $query;
                        
    }

    public function update($table,$data,$where)
	{
		$query = $this->db->where($where)
						->update($table,$data);

		if($query){
			return $query;
		}
	}

	public function search_job($searchData="",$searchDataLocation="")
	{
		$this->db->select("*")
				->from("job_post");
				if($searchData!="")
				{	
					$this->db->like("job_title", $searchData)
							->or_like("company", $searchData);
				}
				
				if($searchDataLocation!="")
				{
					if($searchData!="")
					{
						$this->db->or_like("region",$searchDataLocation)
							->or_like("province", $searchData)
							->or_like("city", $searchData);
					}
					else{
						$this->db->like("region",$searchDataLocation)
							->or_like("province", $searchData)
							->or_like("city", $searchData);
					}
					
				}
				
				$query = $this->db->get();

       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
	}




}
?>